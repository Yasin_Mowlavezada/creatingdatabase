package com.pomtech.yasin.memoriesnotebookdatabase.dataBase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by M.Yasin on 4/22/2017.
 */

public class DataBaseConfig extends SQLiteOpenHelper {

    public static final String DATA_BASE_NAME = "MemoriesDataBase.db";
    public static final int VERSION = 6;

    public static final String TABLE_NAME = "MEMORY_TABLE";

    public String TITLE = "title";
    public String LOCATION = "location";
    public String DATE = "date";
    public String DESCRIPTION = "description";
    public String URI = "uri";
    public String ID = "_id";

    public String CREATE_MEORIES_TABLE = "CREATE TABLE " + TABLE_NAME + " ("
            +ID + " INTEGER PRIMARY KEY AUTOINCREMENT , "
            +TITLE + " TEXT , "
            +LOCATION + " TEXT , "
            + DATE + " TEXT , "
            + DESCRIPTION + " TEXT , "
            + URI + " TEXT " +
            ");";

    public String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";

    public DataBaseConfig(Context context) {
        super(context, DATA_BASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try{
            db.execSQL(CREATE_MEORIES_TABLE);
            Log.d("0077777777", "onCreate: ");
        }catch (Exception e){
            e.printStackTrace();
            Log.d("0077777777", "onCreate: ");

        }


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try{
            db.execSQL(DROP_TABLE);
            onCreate(db);
        }catch (Exception e){

        }
    }
}
