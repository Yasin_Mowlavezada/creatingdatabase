package com.pomtech.yasin.memoriesnotebookdatabase;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.pomtech.yasin.memoriesnotebookdatabase.dataBase.MemoriesFunctions;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new MemoriesFunctions(this);
    }
}
